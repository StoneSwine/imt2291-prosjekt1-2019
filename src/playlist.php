<?php
/**
 * Created by PhpStorm.
 * User: Joakim
 * Date: 02/19/2019
 * Time: 8:05 PM
 */


session_start();
include_once "classes/Video.php";
include_once "classes/Resource.php";
include_once "classes/Playlist.php";
include "../vendor/autoload.php";
require_once "checkAuthentication.php";

/*
 * *********************************************************************************
 * HOW TO  use to error variable
 *
 * Wherever you encounter an error set $error[status] = true;
 * Then you add your message to the messages, like so: array_push($error[messages], "Gonna watch a movie soon");
 * Then when rendering twig, all you have to do is "error"=> $error
 * Magic right? Not really, pretty standard! Happy coding
 * */
$error = array(
    "status" => false,
    "messages" => array()
);
if ($isLoggedIn) {

    $loader = new Twig_Loader_Filesystem(realpath(dirname(__FILE__)) . '/templates');
    $twig = new Twig_Environment($loader, array(//    'cache' => './compilation_cache',
    ));

    /*
     * GET
     * 1 Show alll playlists
     * 2 Show specific playlist
     * */
    if ($_SERVER["REQUEST_METHOD"] == "GET") {

        /* Show specific playlist*/
        if (isset($_GET["id"])) {
            $playlistId = $_GET["id"];
            $playlist = Playlist::setPlaylistId($playlistId);

            try {
                $playlistMeta = $playlist->getMetadata();
                $playlistMeta["subscribed"] = $playlist->isSubscribed($_SESSION['sessionid']);
//            print_r($playlistMeta);
                $playlistMeta["videos"] = $playlist->getPlaylist();
                foreach ($playlistMeta["videos"] as &$video) {
                    $v = Video::withVideoId($video["id"]);

                    $video["thumbnail"] = $v->getThumbnail();
                    //$video = array_merge($video,$v->getThumbnail());
                    $video = array_merge($video, $v->getMetadata());
                }
            } catch (PDOException $e) {
                array_push($error["messages"], "Failed getting playlists.");
            }

            $twig->load('playlist.twig');
            echo $twig->render("playlist.twig",
                [
                    "playlist" => $playlistMeta,
                    "userId" => $_SESSION["sessionid"],
                    "isAdmin" => $isAdmin,
                    "loggedin" => $isLoggedIn,
                    "teacher" => $isTeacher,
                    'title' => 'Schooltube']);
            exit;

        }

        $playlists = array();

        if (isset($_GET["user"])) {
            // Get this users playlist. And only trust the session id
            try {

                $conn = DB::getVideoDBConnection();
                $sql = "SELECT `uuid` AS id, `title`, `description`, `managed_by`, `course_link` FROM PlaylistMeta WHERE `managed_by`= :userid";
                $stmt = $conn->prepare($sql);
                $stmt->bindParam(":userid", $_SESSION["sessionid"]);
                $stmt->execute();

                foreach ($stmt->fetchAll(PDO::FETCH_ASSOC) as &$playlist) {

                    $p = Playlist::setPlaylistId($playlist["id"]);
                    $playlist["subscribed"] = (boolean)$p->isSubscribed($_SESSION['sessionid']);
                    $thumbnail = $p->getThumbnail();
                    //$playlist["thumbnail"] = $thumbnail;
                    $playlist = array_merge($playlist, $p->getThumbnail());
                    //print_r($playlist);

                    array_push($playlists, $playlist);
                }

            } catch (Exception $e) {
                // No metadata could be found
                $error["status"] = true;
                array_push($error["message"] = $e->getMessage());
            }

        } else {
            /* Show all playlists */
            try {

                $conn = DB::getVideoDBConnection();
                $sql = "SELECT `uuid` AS id, `title`, `description`, `managed_by`, `course_link` FROM PlaylistMeta";
                $stmt = $conn->prepare($sql);
                $stmt->execute();
                //echo "<PRE>";

                foreach ($stmt->fetchAll(PDO::FETCH_ASSOC) as &$playlist) {

                    $p = Playlist::setPlaylistId($playlist["id"]);
                    $playlist["subscribed"] = (boolean)$p->isSubscribed($_SESSION['sessionid']);
                    $thumbnail = $p->getThumbnail();
                    //$playlist["thumbnail"] = $thumbnail;
                    $playlist = array_merge($playlist, $p->getThumbnail());
                    //print_r($playlist);

                    array_push($playlists, $playlist);
                }

            } catch (Exception $e) {
                // No metadata could be found
                $error["status"] = true;
                array_push($error["message"] = $e->getMessage());
            }
        }

        /* Get data needed to create a new playlist*/
        // Get all courses
        $courses = array();
        try {
            $conn = DB::getAccountsDBConnection();
            $coursesSQL = "SELECT `course_title` FROM Course";
            $stmt = $conn->prepare($coursesSQL);
            $stmt->execute();
            $courses = $stmt->fetchAll(PDO::FETCH_COLUMN, "course_title");
        } catch (PDOException $e) {
            $error["status"] = true;
            array_push($error["message"] = $e->getMessage());
        }


        echo $twig->render("playlist_overview.twig",
            [
                "playlists" => $playlists,
                "userId" => $_SESSION["sessionid"],
                "isAdmin" => $isAdmin,
                "loggedin" => $isLoggedIn,
                "teacher" => $isTeacher,
                'title' => 'schooltube',
                "courses" => $courses,
                "user"=>$userdata,
                "error" => $error]);
        die();

        /*
         *  Manage changes - add video, remove video, change video position, edit title,
         *  Subscribe/unsubscribe to playlist
         *
         *
         * */
    } else if ($_SERVER["REQUEST_METHOD"] == "POST") {
        // Changes to a playlist goes here

        if (isset($_POST["playlistid"])) {
            $playlistId = $_POST["playlistid"];
            $playlist = Playlist::setPlaylistId($playlistId);


            /* Add video to playlist */
            if (isset($_POST["addvideo"]) && isset($_POST["videoid"])) {
                $videoId = $_POST["videoid"];
                $playlist->addVideo($videoId);
            }
            // Remove from playlist
            if(isset($_POST["remove"]) && isset($_POST["videoid"])){
                $videoId = $_POST["videoid"];
                $playlist->removeVideo($videoId);
            }

            // Delete playlist
            if(isset($_POST["deleteplaylist"])){
                try{
                $playlist->deletePlaylist();
                }catch(PDOException $e){
                    header("Location: error.php?msg=Could not update position on video: " . $e->getMessage());
                }
            }

            if (isset($_POST["subscribe"])) {
                // User wants to subscribe to this playlist
                // We could check what values was passed in, but why would we?
                // if subscribe is not 1, then we are getting targeted by a script kiddie.
                $playlist->addSubscriber($_SESSION['sessionid']);
            }
            if (isset($_POST["unsubscribe"])) {
                // Use wants to unsubsribe from this playlist
                $playlist->removeSubscriber($_SESSION['sessionid']);
            }
            if (isset($_POST["position"]) && isset($_POST["videoid"])) {
                $newPosition = $_POST["position"];
                $videoId = $_POST["videoid"];
                try {
                    $playlist->setPositionOnVideo($newPosition, $videoId);
                } catch (Exception $e) {
                    header("Location: error.php?msg=Could not update position on video: " . $e->getMessage());
                    die();
                }
            }
        } else if (
            isset($_POST["newplaylist"]) &&
            isset($_POST["playlisttitle"]) &&
            isset($_POST["playlistdescription"])
        ) {
            $title = $_POST["playlisttitle"];
            $description = $_POST["playlistdescription"];


            $course = isset($_POST["course"]) ? $_POST["course"] : null;

            $thumbnail = null;
            if (is_uploaded_file($_FILES["thumbnail"]["tmp_name"])) {
                $thumbnail = base64_encode(file_get_contents($_FILES["thumbnail"]["tmp_name"]));
            }
            try {
                $newPlaylist = Playlist::newPlaylist($title, $description, $_SESSION["sessionid"], $course, $thumbnail);
            } catch (PDOException $e) {
                echo $e->getMessage();
                die();
            }
        }


        if (isset($_SESSION["lastGET"])) {
            header("Location: " . $_SESSION["lastGET"]);
            die();
        }
        header("Location: index.php");
        die();
    }
}else{
    header("Location: login.php");
}