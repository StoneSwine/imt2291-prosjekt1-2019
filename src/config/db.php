<?php
/* Configuration for database access */
/* Obviously not these credentials in production... */

/**
 * Return the config for the database:
 * Inspired by: https://stackoverflow.com/questions/14752470/creating-a-config-file-in-php
 *
 */
return array(
    'dbname'   => ["video"=>"schooltube", "accounts"=>"accounts"],
    'host'     => 'database',
    'username' => 'admin',
    'password' => 'admin',
);