<?php
/**
 * Created by PhpStorm.
 * User: Joakim
 * Date: 02/19/2019
 * Time: 3:43 PM
 */

/**
 * Handles comment and rating for a video
 */
session_start();
include "../vendor/autoload.php";
include_once "classes/Video.php";
require_once "checkAuthentication.php";

//echo "hello";
$videoId = "";
//The user is already logged in
if ($isLoggedIn) {

    if ($_SERVER["REQUEST_METHOD"] == "POST") {

        if (isset($_POST["videoid"])) {
            $video = Video::withVideoId($_POST["videoid"]);
            $videoId = $_POST["videoid"];

            // Edit a comment - Not Supported
            if (isset($_POST["comment"]) && isset($_POST["commentid"])) {
                $comment = $_POST["comment"];
                $commentId = $_POST["commentid"];
                editComment($commentId, $comment);

            } // Delete comment
            else if(isset($_POST["commentid"])){
                $commentId = $_POST["commentid"];
                deleteComment($commentId);
            }// Add comment
            else if (isset($_POST["comment"])) {
                $comment = $_POST["comment"];
                addComment($comment);
            }else if(isset($_POST["rating"])){
                /* Rating of video */
                $rank = $_POST["ratingvalue"];
                $video->addRanking($_SESSION['sessionid'], $rank);
            }

            header("Location: watch.php?id=" . $videoId);
            die();
        }

        header("Location: error.php?message=" . "videoid not supplied");
        die();
    }else {
        // We currently does not support GETting comments this way
        header("Location: index.php");
        die();
    }
}
header("Location: index.php");
die();
/**
 * editComment
 *
 * You can only edit your own comment
 * @param $commentId
 * @param $newComment
 */
function editComment($commentId, $newComment)
{
    global $videoId;
    //Check if user owns the comment
    // Not implemented
}

/**
 * deleteComment
 * Deletion of a comment can happen by the
 * owner - The creator of the comment
 * admin - an administrator of the site
 *
 * @param $commentid
 */
function deleteComment($commentid)
{
    //Check if user has permission to delete the comment
    global $videoId, $isAdmin;
    $video = Video::withVideoId($videoId);
    if($isAdmin == true){
        $video->deleteComment($commentid);
    }else{
        try{
            if($video->deleteCommentWithAuthorizationCheck($commentid, $_SESSION["sessionid"])){
                //Successfully deleted
            }else{
                // Failed deleting
            }
        }catch(PDOException $e){
            // Deletion could not be done - the comment might not exist or authorization failed

        }
    }
}

function addComment($comment)
{
    global $videoId;
    // Everyone can leave a comment
    $video = Video::withVideoId($videoId);
    try {
        $video->addComment($comment, $_SESSION["sessionid"], $videoId);
    }catch(PDOException $e){
        // New comment could not be added
        header("Location: error.php?msg=" . $e->getMessage());
        die();
    }
}