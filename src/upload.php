<?php
/**
 * Created by PhpStorm.
 * User: Joakim
 * Date: 02/21/2019
 * Time: 9:56 AM
 */

session_start();
include_once "classes/Video.php";
include_once "classes/Resource.php";
include_once "classes/Playlist.php";
include "../vendor/autoload.php";
require_once "checkAuthentication.php";

/*
 * Upload video files
 *
 * */
$error = array(
    "status" => false,
    "messages" => array()
);

if($isLoggedIn){
    $loader = new Twig_Loader_Filesystem(realpath(dirname(__FILE__)) . '/templates');
    $twig = new Twig_Environment($loader, array(//    'cache' => './compilation_cache',
    ));

    if($_SERVER["REQUEST_METHOD"] == "GET"){

        // Get all courses
        try {
            $conn = DB::getAccountsDBConnection();
            $coursesSQL = "SELECT `course_title` FROM Course";
            $stmt = $conn->prepare($coursesSQL);
            $stmt->execute();
            $courses = $stmt->fetchAll(PDO::FETCH_COLUMN, "course_title");
        }catch(PDOException $e){
            $error["status"] = true;
            $error["messages"] = array_push($error["messages"], "Could not get courses");
        }
        // Get playlist for user
        try{
            $conn = DB::getVideoDBConnection();
            $playlistsSQL = "SELECT `uuid` AS id, `title` FROM PlaylistMeta WHERE `managed_by` = :userId";
            $stmt = $conn->prepare($playlistsSQL);
            $stmt->bindParam(":userId", $_SESSION["sessionid"]);
            $res = $stmt->execute();
            $playlists = $stmt->fetchAll(PDO::FETCH_ASSOC);
        }catch (PDOException $e){
            $error["status"] = true;
            $error["messages"] = array_push($error["messages"], "Could not get playlists");
        }


    }
    elseif($_SERVER["REQUEST_METHOD"] == "POST"){
        /* Upload video */

        if(
            isset($_POST["title"]) &&
            isset($_POST["description"]) &&
            isset($_FILES["video"])
        )
        {
            if(is_uploaded_file($_FILES["video"]["tmp_name"])){
                /* Video is uploaded and we can take it away */
                $title = $_POST["title"];
                $desc = $_POST["description"];
                $course = "";
                if (isset($_POST["course"])){
                    $course = $_POST["course"];
                }

                try {
                    $newVideo = Video::newVideo($_FILES["video"], $title, $desc, $_SESSION["sessionid"], $course);
                }catch (Exception $e){
                    //
                    echo " New video could not be made: " . $e->getMessage();
                    exit;
                }

                if(is_uploaded_file($_FILES["thumbnail"]["tmp_name"])){
                    /* user uploaded thumbnail*/
                    /* Get base64 of it and save to db */
                    $data = file_get_contents($_FILES["thumbnail"]["tmp_name"]);
                    $base64 = base64_encode($data);
                    $newVideo->setThumbnail($base64);
                }

                $tags = array();
                if(isset($_POST["tag"])){
                    $tagsString = $_POST["tag"];
                    $tags = explode(",",$tagsString);
                    foreach ($tags as &$tag){
                        $tag = strtolower($tag); // lowercase
                        $tag = str_replace(' ', '',$tag); // remove spaces
                        try{
                        $newVideo->addTag($tag);
                        }catch(PDOException $e){
                            $error["status"] = true;
                            array_push($error["messages"], "Failed adding tags: " . $tagsString);
                        }
                    }
                }




                $videoId = $newVideo->getVideoId();

                if(isset($_POST["playlist"])){
                    $playlistId = $_POST["playlist"];
                    // TODO: Make sure it is a valid playlist
                    $playlist = Playlist::setPlaylistId($playlistId);
                    $playlist->addVideo($videoId);
                }


                /* After upload take the user to the video*/
                header("Location: watch.php?id=" . $videoId);
            }else {
                /* No file uploaded - fail*/
                $error["status"] = true;
                array_push($error["messages"], "No file was uploaded.");
            }


        }else{
            /* Not enough information was provided */
            $error["status"] = true;
            array_push($error["messages"], "Remember to write title and description. Also provide a file");
        }
    }

    echo $twig->render('upload.twig',[
        "userId"=>$_SESSION["sessionid"],
        "isAdmin"=>$isAdmin,
        "loggedin"=>$isLoggedIn,
        "teacher"=> $isTeacher,
        'title'=>'Schooltube',
        "courses"=>$courses,
        "playlists"=>$playlists,
        "user"=>$userdata,
        "error"=>$error
    ]);
}else{
    // Theres nothing here for non authorized users
    echo "You don't have permission to be here. ";
}
