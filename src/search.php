<?php
/**
 * Created by PhpStorm.
 * User: stone
 * Date: 24.02.19
 * Time: 08:08
 */

session_start();

require_once "checkAuthentication.php";
require_once "classes/Resource.php";
require_once '../vendor/autoload.php';
require_once "classes/Hawkeye.php";
require_once "classes/DB.php";

$util = new Resource();

if($isLoggedIn) {
    //Load TWIG
    $loader = new Twig_Loader_Filesystem(realpath(dirname(__FILE__)) . '/templates');
    $twig = new Twig_Environment($loader, array(//    'cache' => './compilation_cache',
    ));
    if (isset($_POST['submit']) || isset($_GET['query'])) {

        $query = isset($_POST['submit']) ? $_POST['query'] : $_GET['query'];

        $searchclass = new Hawkeye(DB::getVideoDBConnection());
        $result = $searchclass->userSubmittedSearch($query);

        if(count($result) == 0){
            $result = array("message" => "No results found with the query \"" . $_POST['query'] . "\"");
        }

        echo $twig->render("searchresults.html",
            [
                "loggedin"=>$isLoggedIn,
                "teacher"=> $isTeacher,
                "isAdmin" => $isAdmin,
                'title'=>'schooltube',
                "searchresults" => $result,
                "user"=>$userdata,
                "error"=>$error
            ]);

    } else {
        $util->redirect("index.php");
    }
}else{
  $util->redirect("login.php");
}