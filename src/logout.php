<?php
/**
 * Created by PhpStorm.
 * User: stone
 * Date: 12.02.19
 * Time: 12:46
 *
 * This function is inspired from:
 *      https://phppot.com/php/secure-remember-me-for-login-using-php-session-and-cookies/
 *
 *
 * Logs out a user and
 */

require_once "classes/User.php";
require_once "classes/DB.php";

//start session
session_start();

$user = new User(DB::getAccountsDBConnection());

//Clear Session
$_SESSION['sessionid'] = "";
session_destroy();

//clear cookies
$user->clearCookie();

//redirect
header("Location: /");