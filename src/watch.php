<?php
/**
 * Created by PhpStorm.
 * User: Joakim
 * Date: 02/16/2019
 * Time: 8:21 PM
 */

/**
 * Watch a video
 * e.g.: http://localhost/watch.php?id=98069011132055552
 */

session_start();
include_once "classes/Video.php";
include_once "classes/Resource.php";
include_once "classes/Playlist.php";
include "../vendor/autoload.php";
require_once "checkAuthentication.php";


$loader = new Twig_Loader_Filesystem(realpath(dirname(__FILE__)) . '/templates');
$twig = new Twig_Environment($loader, array(//    'cache' => './compilation_cache',
));
$error = array(
    "status" => false,
    "messages" => array()
);

if($isLoggedIn){

    if($_SERVER["REQUEST_METHOD"] == "GET") {
        $error = array();
        if (isset($_GET["id"])) {
            $id = $_GET["id"];


            $video = Video::withVideoId($id);
            try {
                $metadata = $video->getMetadata();
                $rating = $video->getRank();
                $comments = $video->getComments();
                $tags = $video->getTags();
                $playlists = $user->getMyPlaylists(); // ("id","id")

                $userPl = array();
                foreach ($playlists as $playlist) {
                    $pl = Playlist::setPlaylistId($playlist["id"]);
                    array_push($userPl, $pl->getMetadata());
                }

            } catch (Exception $e) {
                // No metadata could be found
                $error["status"] = true;
                $error["message"] = $e->getMessage();
            }


            $watch = $twig->load('watch.twig');
            echo $twig->render("watch.twig",
                [   "playlists"=>$userPl,
                    "tags"=>$tags,
                    "error"=> $_SESSION["error"],
                    "video"=>$metadata,
                    "comments"=>$comments,
                    "rating"=>$rating,
                    "userId"=>$_SESSION["sessionid"],
                    "isAdmin"=>$isAdmin,
                    "loggedin"=>$isLoggedIn,
                    "user"=>$userdata,
                    'title'=>'schooltube',
                    "teacher"=> $isTeacher
                ]);
            unset( $_SESSION["error"]);

        }
    }else if($_SERVER["REQUEST_METHOD"] == "POST") {

        if (isset($_POST["videoid"])) {
            $videoid = $_POST["videoid"];
            $video = Video::withVideoId($videoid);

            // Only owner of administrator can do these operations
            if ($isAdmin || $video->getOwner() == $_SESSION["sessionid"]) {

                // Delete the video
                if (isset($_POST["delete"])) {
                    // Delete the video
                    try {
                        $video->deleteMe();
                    } catch (PDOException $e) {
                        $error[status] = true;
                        array_push($error["messages"], "Could not delete video");
                    }
                }

                // Update title
                if (isset($_POST["videotitle"])) {
                    $newTitle = $_POST["videotitle"];
                    if(strlen($newTitle) > 0){
                        $video->setTitle($newTitle);
                    }
                }
                // Update description
                if (isset($_POST["videodescription"])) {
                    $newDescription = $_POST["videodescription"];
                    if(strlen($newDescription) > 0) {
                        $video->setDescription($newDescription);
                    }
                }
                //Update tags
                // Remove all tags and add the new tags one by one
                if(isset($_POST["videotags"])){
                    $newTags = $_POST["videotags"];
                    $video->removeAllTags();
                    $tags = explode(",",$newTags);
                    foreach ($tags as &$tag){
                        $tag = strtolower($tag); // lowercase
                        $tag = str_replace(' ', '',$tag); // remove spaces
                        try{
                            $video->addTag($tag);
                        }catch(PDOException $e){
                            $error["status"] = true;
                            array_push($error["messages"], "Failed adding tags: " . $tagsString);
                        }
                    }
                }
            } else {
                $error[status] = true;
                array_push($error["messages"], "You are not authorized to do that.");
            }
        }
        $_SESSION["error"] = $error;
        header("Location: watch.php?id=" . $videoid);
    }

}else{
    header("Location: login.php");
}

function loadSite(){

}