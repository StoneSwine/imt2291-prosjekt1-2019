<?php
/**
 * Created by PhpStorm.
 * User: Joakim
 * Date: 02/17/2019
 * Time: 4:18 PM
 */

/**
 * stream - stream a video file to client
 *
 *
 */

include_once "config/video_config.php";
include_once "classes/VideoStream.php";

if(isset($_GET["id"])){
    $id = $_GET["id"];
    /*Check if we actually have that file*/

    $filePath = VIDEO_S3_ROOT . $id . ".mp4";
    if(file_exists($filePath)){
        $stream = new VideoStream($filePath);
        $stream->start();

     /*   $fileSize = filesize($filePath);
        $mime = mime_content_type($filePath);
        header('Content-Description: Stream');
        header("Cache-Control: max-age=2592000, public");
        header("Expires: ".gmdate('D, d M Y H:i:s', time()+2592000) . ' GMT');
        header("Last-Modified: ".gmdate('D, d M Y H:i:s', @filemtime($this->path)) . ' GMT' );

        header("Content-Type: $mime");
        header("Content-Length: $fileSize");

        readfile($filePath);*/
    }else{
        echo "File at: " . $filePath . " doesnt exist";
    }

}

//header("HTTP/1.0 404 Not Found");