<?php
/**
 * Created by PhpStorm.
 * User: Magnus
 * Date: 19.02.19
 * Time: 18:49
 */

session_start();
require_once '../vendor/autoload.php';
require_once "checkAuthentication.php";
require_once "./classes/Administrator.php";
require_once "./classes/DB.php";
require_once "./classes/Resource.php";
//Load TWIG
$loader = new Twig_Loader_Filesystem(realpath(dirname(__FILE__)) . '/templates');
$twig = new Twig_Environment($loader, array(//    'cache' => './compilation_cache',
));

//TODO: validate admin user... (and pass it to the twig ?)

$admin = new Administrator(DB::getAccountsDBConnection());
$util = new Resource();

//Verify that the user is an admin and is logged in
if ($isLoggedIn && $isAdmin) {

//If the form with the checkboxes is submitted
    if (isset($_POST['submit_checkbox'])) {
        //loop through the key and value pair of the checkboxes
        foreach ($_POST['checkbox'] as $useremail => $key) {
            //the checkbox is checked
            if ($key) {
                //Set the users status to teacher
                $admin->setUserAsTeacher($useremail);

                //reset the request for the users that are not accepted as teachers
            } else {
                $admin->setUserAsStudent($useremail);
            }
        }
        //Render the page again
        $util->redirect("adminpage.php");

        //if the search item is not empty
    } elseif (isset($_POST['submit_search']) && !empty($_POST['search_text'])) {
        //try to get the full username or email
        $searchtext = $_POST['search_text'];

        //try to get the full email
        if ($result = $admin->getUserByUsername($searchtext)) {
            echo $twig->render("base.html", ["loggedin" => $isLoggedIn, "admin" => $isAdmin, "teacher"=> $isTeacher,'title' => 'schooltube', "showadminpage" => 1, "searchResults" => $result]);
        }//try to get the full first- and lastname of the user
        else if ($result = $admin->getUserByFirstAndOrLastname($searchtext)) {
            echo $twig->render("base.html", ["loggedin" => $isLoggedIn, "admin" => $isAdmin, "teacher"=> $isTeacher,'title' => 'schooltube', "showadminpage" => 1, "searchResults" => $result]);
        }//try to get either the first or the last name of the user
        else {
            $util->redirect("adminpage.php");
        }

        //selected user to be promoted as admin
    } else if ($_GET['setasadmin']) {
        $userid = $_GET['setasadmin'];

        //make sure the user has a role which is not admin
        $role = $admin->getRoleByUserID($userid);
        if($role != "" && $role != "admin") {
            //promote the user to admin (swap the roles)
            $admin->changeUserRole($userid, $admin->getRoleByUserID($userid), "admin");
        }
        $util->redirect("adminpage.php");

    } //No forms are submitted: show the page
    else {
        // display the normal page
        $users = $admin->getUsersThatWantsToBeTeacher();
        echo $twig->render("base.html", ["loggedin" => $isLoggedIn, "isAdmin" => $isAdmin, "teacher"=> $isTeacher, 'title' => 'schooltube', "showadminpage" => 1, "names" => $users]);
    }
}