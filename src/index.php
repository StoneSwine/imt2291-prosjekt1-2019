<?php

session_start();
require_once '../vendor/autoload.php';
require_once "checkAuthentication.php";
require_once "classes/Administrator.php";
include_once "classes/Playlist.php";

/*
 * *********************************************************************************
 * HOW TO  use to error variable
 *
 * Wherever you encounter an error set $error[status] = true;
 * Then you add your message to the messages, like so: array_push($error[messages], "Gonna watch a movie soon");
 * Then when rendering twig, all you have to do is "error"=> $error
 * Magic right? Not really, pretty standard! Happy coding
 * */
$error = array(
    "status" => false,
    "messages" => array()
);
/*********************************************************************************/
if($isLoggedIn){
    //Load TWIG
    $loader = new Twig_Loader_Filesystem(realpath(dirname(__FILE__)) . '/templates');
    $twig = new Twig_Environment($loader, array(//    'cache' => './compilation_cache',
    ));

    /* Get top videos */
    try{
        $conn = DB::getVideoDBConnection();

        $sql = "SELECT `uuid` AS id, `title`, `description`, `thumbnail` FROM VideoMetadata WHERE `uuid` IN (SELECT `video_ref` FROM VideoRank GROUP BY `video_ref` ORDER BY AVG(`rankValue`)) LIMIT 10";
        $stmt = $conn->prepare($sql);
        $stmt->execute();

        $videos = $stmt->fetchAll(PDO::FETCH_ASSOC);


        if($isAdmin){ //Create an alert if there are any new requests for users that wants to be a teacher
            $admin = new Administrator(DB::getAccountsDBConnection());
            if ($admin->getUsersThatWantsToBeTeacher()) {
                $isteacherRequests = true;
            } else {
                $isteacherRequests = false;
            }
        }

    }catch(PDOException $e){
        $error["status"] = true;
        array_push($error["messages"],"Could not get top videos. Sorry about this.");
    }

    /* Show subscribed playlists*/
    $playlists = array();
    try {

        $conn = DB::getVideoDBConnection();
        $sql = "SELECT `uuid` as id, `title`, `description`, `managed_by`, `course_link` FROM PlaylistMeta WHERE `uuid` IN (SELECT `playlist_ref` FROM PlaylistSubscription WHERE `user_ref` = :userid)";
        $stmt = $conn->prepare($sql);
        $stmt->bindParam(":userid", $_SESSION["sessionid"]);
        $stmt->execute();

        foreach ($stmt->fetchAll(PDO::FETCH_ASSOC) as &$playlist){

            $p = Playlist::setPlaylistId($playlist["id"]);
            $playlist["subscribed"] = (boolean) $p->isSubscribed($_SESSION['sessionid']);

            $playlist = array_merge($playlist, $p->getThumbnail());
            //print_r($playlist);

            array_push($playlists,$playlist);
        }

    }catch(Exception $e){
        // No metadata could be found
        $error["status"] = true;
        array_push($error["messaged"], "Could not fetch playlist");
    }



    echo $twig->render("index.twig",
        [
            "playlists"=>$playlists,
            "loggedin"=>$isLoggedIn,
            "teacher"=> $isTeacher,
            "isAdmin" => $isAdmin,
            'title'=>'schooltube',
            "videos"=>$videos,
            "alert" => $isteacherRequests,
            "user"=>$userdata,
            "error"=>$error
        ]);
}else{
    header("Location: login.php");
}




