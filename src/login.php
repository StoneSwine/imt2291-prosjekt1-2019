<?php
/**
 * Created by PhpStorm.
 * User: stone
 * Date: 12.02.19
 * Time: 09:47
 *
 * <CREDITS>
 *      https://phppot.com/php/secure-remember-me-for-login-using-php-session-and-cookies/
 *      https://paragonie.com/blog/2015/04/secure-authentication-php-with-long-term-persistence
 * </CREDITS>
 *
 */
//start session
session_start();

//Requirements
require_once "classes/User.php";
require_once "classes/Resource.php";
require_once "classes/DB.php";
require_once '../vendor/autoload.php';

//Load TWIG
$loader = new Twig_Loader_Filesystem(realpath(dirname(__FILE__)) . '/templates');
$twig = new Twig_Environment($loader, array(//    'cache' => './compilation_cache',
));

$util = new Resource();
$user = new User(DB::getAccountsDBConnection());

//Check if the user is already logged in (existing session OR valid cookies)
//Get the login-status of the user
require_once "checkAuthentication.php";

//The user is already logged in
if ($isLoggedIn) {
    //render member page
    $util->redirect("/");

} else if (isset($_POST['submit'])) {//The form is submitted

    //Get the credentials from the form
    $credentials = array(
        'username' => $_POST['inputEmail'],
        'password' => $_POST['inputPassword'],
    );

    //try to get the user with the given username from the database
    $currentuser = $user->getUserByUsername($credentials['username']);
    //get the first user
    $currentuser = $currentuser[0];
    //If a user if found in the database and the user is verified
    if ($currentuser && password_verify($credentials['password'], $currentuser["password_hash"])) {

        // Set Cookie in browser if 'Remember Me' is checked
        if (isset($_POST["rememberPassword"])) {

            //Generate new selector token
            $selector = $util->generateToken(12);

            //Generate new validator token
            $validator = $util->generateToken(24);

            //Generate hashed-validator for the database according best practice
            $hashedvalidator = hash("sha256", $validator);

            // Generate and set the cookie:
            // Cookie-format:
            //      remember_me=<SELECTOR>:<VALIDATOR>
            $cookie = $selector . ":" . $validator;
            setcookie("remember_me", $cookie, $cookie_expiration_time);

            //TODO: create new cookie and delete the old ones

            //get some data from the session
            $expirationdate = date("Y-m-d H:i:s", $cookie_expiration_time);
            $useragent = $_SERVER['HTTP_USER_AGENT'];
            $userip = $util->getUserIP();

            //Delete existing token/cookie in the database
            $user->deleteToken($currentuser['userId']);
            //insert a new token in the database
            $user->insertToken($currentuser['userId'], $selector, $hashedvalidator, $expirationdate, $useragent, $userip);
        }

        //set the session ID if the user is logged in correctly AND it is not set already
        if (!isset($_SESSION["sessionid"])) {
            $user->setSessionId($currentuser["userId"]);
        }

        $util->redirect("/");

    } else {
        //Render page with invalid login message
        $errortext = array("text" => "Invalid username or password");
        echo $twig->render("login.html", array("message" => $errortext));
    }

//The login-form is not submitted AND the user is not logged in from before
} else {
    echo $twig->render("login.html");
}

