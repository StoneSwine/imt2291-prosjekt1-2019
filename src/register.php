<?php
/**
 * Created by PhpStorm.
 * User: stone
 * Date: 12.02.19
 * Time: 09:47
 */

//Requirements
require_once "classes/User.php";
require_once '../vendor/autoload.php';
require_once "classes/Resource.php";
require_once "classes/DB.php";

//Load TWIG
$loader = new Twig_Loader_Filesystem('./templates');
$twig = new Twig_Environment($loader, array(//    'cache' => './compilation_cache',
));

session_start();

$user = new User(DB::getAccountsDBConnection());
$util = new Resource();

//Check if the user is already logged in (existing session OR valid cookies)
//Get the login-status of the user
require_once "checkAuthentication.php";

if ($isLoggedIn) {
    $util->redirect("/");
}
//The user have submitted a form, and is not logged in
if (isset($_POST['submit'])) {

    //Get the credentials
    $credentials = array(
        'email' => $_POST['inputEmail'],
        'fname' => $_POST['inputFirstname'],
        'lname' => $_POST['inputLastname'],
        'password' => $_POST['inputPassword'],
        'confirmPassword' => $_POST['inputConfirmPassword'],
        'isTeacher' => isset($_POST['techerCheckbox']) ? 1 : 0,
    );

    /**
     * Make sure that all the fields in the form have content
     * need strlen, because empty() treat '0' as false
     */
    foreach($credentials as $field) {
        if (empty($field) && !strlen($field)){
            $errortext = array("text" => "All fields are required");
            echo $twig->render("register.html", array("message" => $errortext));
            die();
        }
    }

    // Verify email
    if (!$user->verifyUniqueEmail($credentials['email'])) {
        $errortext = array("text" => "Invalid email address");
        echo $twig->render("register.html", array("message" => $errortext));
        die();
    }

    // Verify that the two passwords match
    if (!$user->verifyPasswords($credentials['password'], $credentials['confirmPassword'])) {
        $errortext = array("text" => "The passwords doesn't match");
        echo $twig->render("register.html", array("message" => $errortext));
        die();
    }

    // Insert to database
    // Get the last inserted id for the user
    $id = $user->insertUser($credentials['email'], $credentials['password'], $credentials['isTeacher'], $credentials['fname'], $credentials['lname']);

    //The user can not be inserted
    if ($id == 0) {
        $errortext = array("text" => "Something wrong happened, this is on us. Please be patient and try again");
        echo $twig->render("register.html", array("message" => $errortext));
        die();
    }

    // Login user --> set session variabels (TODO: make a function which does this all over the place)
    $user->setSessionId($id);

    // redirect
    $util->redirect("/");

} else {
    echo $twig->render("register.html");
}