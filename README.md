# Prosjekt 1 IMT2291 våren 2019

## Prosjektdeltakere
* Joakim N. Ellestad (141245)
* Magnus Lien Lilja  (473150)

# Oppgaveteksten
Oppgaveteksten ligger i [Wikien til det originale repositoriet](https://bitbucket.org/okolloen/imt2291-prosjekt1-2019/wiki/Home).

# Prosjekt setup for utvikling (Ikke relevant ved sensur)
Ref: [jetbrains: Configuring Folders Within a Content Root](https://www.jetbrains.com/help/phpstorm/configuring-folders-within-a-content-root.html)
Ref: [Project structure php-pds](https://stackoverflow.com/a/42393990/3904492)

`/database` Inneholder filer for oppsett og dummy data for database **Excluded**

`/docker` Filer for å få docker miljø opp for testing. **Excluded**

`/src` Kildefiler. php, html, css. **Source**

`/src/config` Konfigurasjonsfiler. F.eks. påloggingsinformasjon for database **Source**

`/src/public` Filer som er offentlige(ugradert), dvs. gjør ingenting om de er eksponert. Type statiske bilder og annet. **Source**

`/test` Alle tester skrives her. **Test Source Root**


## volume for persistent data - `schooltube`

`docker volume create schooltube`, vil bli lagd ved docker-compose up om den ikke finnes fra før.

I `docker-compose.yml` vil volumet bli mount til `/storage` på rotkatalogen. For videoer som blir lastet opp er filbanen til den `/storage/video`.
 
Hvis kopiering av video fila ikke fungerer gjør det manuelt slik(forskjell på windows og unix filbaner ved cp kommando "/" "\" ):
```bash
docker cp .\media\* imt2291-prosjekt1-2019_webserver_1:/storage/video/
```

# Rapport

Tjenestenavn: **Schooltube**

**Steg 1 - oppsett**

Prosjektet er puttet i docker containere. `webserver`, `database`, `test`, `selenium-hub`, `chrome`, `firefox`. Det benyttes i tillegg et volum kalt `storage`, dette burde bli automatisk laget og filer kopiert over ved `docker-compose up --build`.

  
Prosjektet kan startes med `docker-compose up`. NB! Kan ta litt tid, så hent en kaffe.

**Svært viktig**

* `www-data` er brukeren som kjører apache, denne må ha skriverettigheter til volumet `storage`. I Dockerfile skal dette egentlig lages, men vi har ikke hatt konsistens i 
at dette har skjedd. Så før du kjører tester eller prøver nettside: Hopp inn i enten test eller webserver `docker exec imt2291-prosjekt1-2019_webserver_1 bash` og
hvis ikke rettighetene er slik:
```
root@f310450aa080:/var/www/html/test# ls -alt /storage/
total 12
drwxrwx--- 2 www-data www-data 4096 Mar  1 12:14 video
drwxrwx--- 3 www-data www-data 4096 Mar  1 12:11 .
drwxr-xr-x 1 root     root     4096 Mar  1 12:11 ..
```
så bruk kommandoene: `chown www-data:www-data /storage -R` og `chmod 770 /storage -R`. 

**For å kjøre tester**

Test er på en egen docker. Finn denne med `docker container ls`, den vil hete `imt2291-prosjekt1-2019_test_1`

Bruk `docker exec -it imt2291-prosjekt1-2019_test_1 bash` for å få terminal inn til containeren.

Kjør testene med `codecept run unit` og `codecept run acceptance` for den funksjonelle testen. Den funksjonelle testen har ikke brukt tilfeldig tittel, beskrivelse etc. og ved gjentatt testing kan nettsiden se ut som den har duplikater av videoer og spillelister.
Det har den "egentlig" ikke. Spillelister og videoer har unike id-er(UUID_SHORT), samme tittel er jo tillatt. 
 
**Se nettsiden**

Docker eksponerer port `80` på localhost for webserver, port `8000` for database, der login er `admin`:`admin`.
Ved første gangs besøk på nettsiden finnes det to brukere på systemet.

(brukernavn:passord)

`kyrre@docker.com`:`admin` -> er lærer, men ikke admin, har to spillelister `infrastructure` med 1 video og `Silicon Valley` med 2 videoer.

`example@mail.com`:`admin`(Øyvind) -> er lærer og admin, har en spilleliste kalt www-teknologi, men ingen videoer. 



## Forbedringspunkter
* Ved `My Videos` brukes søkemekanismen for å finne brukeren sine videoer. Denne bruker nå etternavnet på brukeren for å finne sine videoer, men dette er litt uheldig. Forbedring vil være at søkemekanismen kan ta i mot en bruker sin id. Det gjør søket mer nøyaktig.
* Måten `src/classes/Video.php` er laget må en ny Video være laget ved en POST request siden `move_uplaoded_file` brukes inni der og denne feiler og lager en throw. Dette gjør `test/tests/unit/VideoTest.php` vanskelig da, vi aldri får returnet et Video objekt vi kan jobbe med. Videre arbeid vil være å endre måten et nytt Video objekt blir laget. Det kan være å separere ut flytting av fil fra `Video::newVideo`, da vil et Video objekt bli returnet og vil være lettere å teste mot. 
`src/classes/Video.php` blir jo godt testet med den funksjonelle testen også egentlig.

## NB
* `src/classes/VideoStream.php` er copy/pasta fra [codesamplez.com](http://codesamplez.com/programming/php-html5-video-streaming-tutorial) for å få streaming til å fungere.

## Punkter vi er spesielt førnyd med

* **Forespørsel om å bli lærer** For å få tak i både eposten og svaret på om den personen blir lærer eller ikke via checkboxer, så sender jeg alt tilbake i en array.
Der er indexen eposten til læreren og verdien er en bool på om de blir lærer eller ikke. Dette gjør det lett i backend og filtrere ut de som har blitt godkjent.
```html
<input type="hidden" name="checkbox[heisann@hei.com]" value="0">
<p class="onoff">
    <input type="checkbox" name="checkbox[heisann@hei.com]" value="1" id="1">
</p>
```

* **Søkefunksjonen** For å søke opp videoer og spillelister med forskjellige filtere har jeg laget en klasse "Hawkeye.php" 
som jeg er ganske fornøyd med funksjonaliteten til

* Tok noen timer å komme fram til en god sql "setning" som kunne oppdatere posisjonen til videoer i en spilleliste. Se `src/database/03_procedures.sql` og tilhørende kaller `src/classes/playlist.php:330`