<?php

require_once realpath(dirname(__FILE__)) . "/../../../src/classes/DB.php";
require_once realpath(dirname(__FILE__)) . "/../../../src/classes/User.php";
require_once realpath(dirname(__FILE__)) . "/../../../src/classes/Administrator.php";
require_once realpath(dirname(__FILE__)) . "/../../../src/classes/Resource.php";
require_once realpath(dirname(__FILE__)) . "/../../../src/classes/Video.php";
require_once realpath(dirname(__FILE__)) . "/../../../src/classes/Hawkeye.php";


class HawkeyeTest extends \Codeception\Test\Unit
{
    /**
     * @var \UnitTester
     */
    protected $tester;
    private $tmpuseremail;
    private $tmpdata;
    private $title_video1;
    private $title_video2;
    private $id;
    private $testvideo;
    private $user;
    private $tmpuserfirstname;
    private $tmpuserlastname;
    private $tmpcourse;
    private $randomfactor;
    private $videoobjects = [];

    protected function _before()
    {
        //dummy file upload
        $this->testvideo = array(
            "name" => "samplevideo.mp4",
            "type" => "video/mp4",
            "tmp_name" => "/var/www/html/test/tests/_data/samplevideo.mp4",
            "error" => 0,
            "size" => 1055736
        );

        $this->user = new User(DB::getAccountsDBConnection());
        $util = new Resource();
        $admin = new Administrator(DB::getAccountsDBConnection());

        //gernerate testdata
        $this->tmpdata = "This field doesnt matter in this test";
        $this->tmpuseremail = $util->generateToken(10);
        $this->tmpuserfirstname = $util->generateToken(10);
        $this->tmpuserlastname = $util->generateToken(10);

        //genreate data for the uploaded test videoes
        $this->randomfactor = $util->generateToken(5);
        $this->title_video1 = $this->randomfactor . " " .  $util->generateToken(10);
        $this->title_video2 = $this->randomfactor . " " . $util->generateToken(10);
        $this->tmpcourse = $util->generateToken(5);

        //create a new user and set as teacher
        $this->id = $this->user->insertUser($this->tmpuseremail, $this->tmpdata, 1, $this->tmpuserfirstname, $this->tmpuserlastname);
        $admin->setUserAsTeacher($this->tmpuseremail);

        //upload the first video
        try {
            $this->videoobjects[] = Video::newVideo($this->testvideo, $this->title_video1, $this->tmpdata, $this->id);
        }catch (Exception $e){
            echo $e;
        }

        //upload a second video
        try {
            $this->videoobjects[] = Video::newVideo($this->testvideo, $this->title_video2, $this->tmpdata, $this->id, $this->tmpcourse);
        }catch (Exception $e){

        }
    }

    //Delete the video and the user after the test is finished
    protected function _after(){
        $this->user->deleteUser($this->id);
        foreach ($this->videoobjects as $video){
            $video->deleteMe();
        }
    }

    /**
     * Test to see if the search-queries return the expected results
     * The tests are performed with the initial database setup already in place
     */
    public function testUserSubmittedSearch(){

        //try to search for videos with the lecturers firstname
        $searchclass = new Hawkeye(DB::getVideoDBConnection());
        $result = $searchclass->userSubmittedSearch("type:video lecturer:".$this->tmpuserfirstname);
        $this->assertEquals(2, count($result));

        //try to search for videoes with the coursname
        $searchclass = new Hawkeye(DB::getVideoDBConnection());
        $result = $searchclass->userSubmittedSearch("course:".$this->tmpcourse);
        $this->assertEquals(1, count($result));

        //try to find playlist made by the lecurers lastname
        $searchclass = new Hawkeye(DB::getVideoDBConnection());
        $result = $searchclass->userSubmittedSearch("type:playlist lecturer:" . $this->tmpuserlastname);
        $this->assertEquals(0, count($result));


        //try to search for wildcard
        $searchclass = new Hawkeye(DB::getVideoDBConnection());
        $result = $searchclass->userSubmittedSearch($this->randomfactor);
        $this->assertEquals(2, count($result));

        //try to search for the whole description:
        $searchclass = new Hawkeye(DB::getVideoDBConnection());
        $result = $searchclass->userSubmittedSearch("\"" . $this->title_video1 . "\"");
        $this->assertEquals(1, count($result));
    }
}