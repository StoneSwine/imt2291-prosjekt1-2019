<?php

require_once realpath(dirname(__FILE__)) . "/../../../src/classes/DB.php";
require_once realpath(dirname(__FILE__)) . "/../../../src/classes/User.php";
require_once realpath(dirname(__FILE__)) . "/../../../src/classes/Administrator.php";


class AdministratorTest extends \Codeception\Test\Unit
{
    /**
     * @var \UnitTester
     */
    protected $tester;
    private $admin;
    private $user;
    private $tmpdata = "**testdata**";
    private $id;

    /**
     * Create a known state of users
     */
    protected function _before()
    {
        //create the objects
        $this->user = new User(DB::getAccountsDBConnection());
        $this->admin = new Administrator(DB::getAccountsDBConnection());

        //insert a temporary user
        $this->id = $this->user->insertUser($this->tmpdata, $this->tmpdata, 1, $this->tmpdata, $this->tmpdata);
    }

    /**
     * Clean up the database
     */
    protected function _after()
    {
        $this->user->deleteUser($this->id);
    }


    /**
     * Change the user role test
     */
    public function testChangeUserRole()
    {
        //Get the current user role
        $this->assertEquals("student", $this->user->getRoleByUserID($this->id));
        //Change the role
        $this->admin->changeUserRole($this->id, "student", "admin");
        //verify the change
        $this->assertEquals("admin", $this->user->getRoleByUserID($this->id));
    }

    /**
     * Test to set user as a student
     */
    public function testSetUserAsStudet(){
        //Change the role
        $this->admin->changeUserRole($this->id, "student", "admin");

        //verify that the user is an admin
        $this->assertEquals("admin", $this->user->getRoleByUserID($this->id));

        //change the user role
        $this->admin->setUserAsStudent($this->tmpdata);

        //verify that the user is an admin
        $this->assertEquals("student", $this->user->getRoleByUserID($this->id));
    }
}