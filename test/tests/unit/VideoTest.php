<?php
require_once realpath(dirname(__FILE__)) . "/../../../src/classes/Video.php";

/**
 * Class VideoTest
 * test for methods in video class works as inteded
 *
 * TODO: Change the way Video.php creates a new object. move_uploaded_file doesn't work for non POST files.
 * -> doesnt work when testing.
 */
class VideoTest extends \Codeception\Test\Unit
{
    /**
     * @var \UnitTester
     */
    protected $tester;
    
    protected function _before()
    {
    }

    protected function _after()
    {
    }

    private $video;
    // tests
    public function testNewVideo()
    {

        //dummy file upload
        $testvideo = array(
            "name" => "samplevideo.mp4",
            "type" => "video/mp4",
            "tmp_name" => "/var/www/html/test/tests/_data/samplevideo.mp4",
            "error" => 0,
            "size" => 1055736
        );

        $util = new Resource();
        $title = $util->generateToken(10);
        $description = $util->generateToken(10);
        $owner = 2;

        codecept_debug(exec("id"));
        try {

            $video = Video::newVideo($testvideo, $title, $description, $owner, "");
            $this->tester->assertInstanceOf(Video::class, $video,"Video class");

            $videoid = $video->getVideoID();
            codecept_debug($videoid);
            $this->tester->assertNotEquals("",$videoid);
        }catch (Exception $e){
             codecept_debug($e->getMessage());
        }


        //$getVideo = Video::withVideoId($videoid);
        /*$metadata = $video->getMetadata();
        $this->tester->assertEquals($title, $metadata["title"]);
        $this->tester->assertEquals($description, $metadata["description"]);
        $this->tester->assertEquals($videoid, $metadata["id"]);
        $this->tester->assertEquals($owner, $metadata["owner"]);*/

    }
}