<?php
require_once realpath(dirname(__FILE__)) . "/../../../src/classes/DB.php";
require_once realpath(dirname(__FILE__)) . "/../../../src/classes/User.php";
require_once realpath(dirname(__FILE__)) . "/../../../src/classes/Resource.php";

/**
 * @property string validemail
 */
class UserTest extends \Codeception\Test\Unit
{
    /**
     * @var \UnitTester
     */

    //Test variables: login tests
    protected $tester;
    private $user;
    private $util;
    private $validuserid, $unvaliduserid;
    private $validselector, $unvalidselector;
    private $validator;
    private $misctext;
    private $validexpirationdate;
    private $validusername;
    private $unvalidusername;
    private $hashedvalidator;
    private $unvalidexpirationdate;
    private $validcurrenttime;
    private $unvalidcurrenttime;

    //Test variables: register tests
    private $unvalidemail1;
    private $unvalidemail2;
    private $password;
    private $isTeacher;
    private $validEmail;
    private $tmpname;


    private $id;
    private $tmpdata = "this doesn't matter in the test";
    /**
     * Generate stuff\
     */
    protected function _before()
    {

        /*
         *   CREATE VARIABLES FOR LOGIN TESTS
         */
        $this->user = new User(DB::getAccountsDBConnection());
        $this->util = new Resource();

        //generate username
        $this->validusername = "example@mail.com";
        $this->unvalidusername = $this->util->generateToken(20);

        //generate userid
        $this->validuserid = 1;
        $this->unvaliduserid = 0;

        //generate selector
        $this->validselector = $this->util->generateToken(12);
        $this->unvalidselector = $this->util->generateToken(100);

        //generate validators
        $this->validator = $this->util->generateToken(12);
        $this->hashedvalidator = hash("sha256", $this->validator);


        $this->validexpirationdate = date("Y-m-d H:i:s", (time() + 10));
        $this->unvalidexpirationdate = date("Y-d H", (time() + 10));

        $this->validcurrenttime = date("Y-m-d H:i:s", (time() - 100));
        $this->unvalidcurrenttime = date("Y-m-d H:i:s", (time() + 100));

        $this->misctext = "This field doesn't matter in this test";

        /*
         *   CREATE VARIABLES FOR REGISTER USER TESTS
         */
        //Generate emails
        $this->validEmail = $this->util->generateToken(5) . "@mail.com";
        $this->unvalidemail1 = "example@mail.";
        $this->unvalidemail2 = "examplemail.com";

        //Generate misc variables
        $this->password = "123456789";
        $this->isTeacher = 0;
        $this->tmpname  = "test";

        //insert a temporary user
        $this->id = $this->user->insertUser($this->tmpdata, $this->tmpdata, 1, $this->tmpdata, $this->tmpdata);


    }

    protected function _after()
    {
        //Delete the token
        $this->user->deleteToken($this->validuserid);
        //delete the insered user
        $this->user->deleteUser($this->id);
    }

    /**
     * Test to see if a users cookie is valid with the exisiting one in the database
     */
    public function testValidateCookie()
    {

        //Insert a cookie in the database
        $this->testInsertToken();

        //try to validate a cookie with a selector that doesn't exist
        $this->assertFalse($this->user->validateCookie($this->unvalidselector, $this->validator, $this->validcurrenttime));


        //try to validate with an expired cookie
        $this->assertFalse($this->user->validateCookie($this->validselector, $this->validator, $this->unvalidcurrenttime));


        //try to validate a valid cookie
        $this->assertTrue($this->user->validateCookie($this->validselector, $this->validator, $this->validcurrenttime));


    }

    /**
     * Test to select a user from the database by quering the username
     */
    public function testGetUserByUsername()
    {
        //Try to get a user which doesn't exist
        $this->assertFalse($this->user->getUserByUsername($this->unvalidusername));

        //Get a valid user, and check if the returning array has one element in the array
        $this->assertArrayHasKey(0, $this->user->getUserByUsername($this->validusername));
    }

    /**
     * Test to see if we can insert a new token to the database
     */
    public function testInsertToken()
    {
        //try to insert a token with a unvalid user-id
        $this->assertFalse($this->user->insertToken($this->unvaliduserid, $this->validselector, $this->hashedvalidator, $this->validexpirationdate, $this->misctext, $this->misctext));

        //try to insert a token with a too long selector
        $this->assertFalse($this->user->insertToken($this->validuserid, $this->unvalidselector, $this->hashedvalidator, $this->validexpirationdate, $this->misctext, $this->misctext));

        //try to insert a token with an unvalid expiration-date format
        $this->assertFalse($this->user->insertToken($this->validuserid, $this->unvalidselector, $this->hashedvalidator, $this->unvalidexpirationdate, $this->misctext, $this->misctext));

        //try to insert a valid token
        $this->assertTrue($this->user->insertToken($this->validuserid, $this->validselector, $this->hashedvalidator, $this->validexpirationdate, $this->misctext, $this->misctext));
        
    }

    /**
     * Test to get the role of the user by querying the user-id
     */
    public function testGetRoleByUserID()
    {
        //unvalid user-id
        $this->assertEquals($this->user->getRoleByUserID($this->unvaliduserid), "");

        //valid user-id
        $this->assertEquals("student", $this->user->getRoleByUserID($this->id));
    }

    /**
     * test to see if the two password fields in the register form match
     */
    public function testVerifyPasswords()
    {

        //totaly different passwords
        $this->assertFalse($this->user->verifyPasswords($this->password, $this->validEmail));

        //Test case sensitiv
        $this->assertFalse($this->user->verifyPasswords("Onlyoneletterdifference", "onlyoneletterdifference"));

        //two equal passwords
        $this->assertTrue($this->user->verifyPasswords($this->password, $this->password));

    }

    /**
     * Test to see if the email is valid and unique
     */
    public function testVerifyUniqueEmail()
    {

        // Try to verify a user with an unvalid email address
        $this->assertFalse($this->user->verifyUniqueEmail($this->unvalidemail1));

        // Try to verify a user with an existing email address
        $this->assertFalse($this->user->verifyUniqueEmail($this->unvalidemail2));

        // Try to create a user with a good email
        $this->assertTrue($this->user->verifyUniqueEmail($this->validEmail));;
    }

    /**
     * Test to insert a new user to the database
     */
    public function testInsertUser()
    {
        //swap the parameters
        $this->assertEquals($this->user->insertUser($this->validEmail, $this->isTeacher, $this->password, $this->tmpname, $this->tmpname), 0);

        //Blank email adress
        $this->assertEquals($this->user->insertUser(null, $this->password, $this->isTeacher, $this->tmpname, $this->tmpname), 0);

        //Valid test
        $userid = $this->user->insertUser($this->validEmail, $this->password, $this->isTeacher , $this->tmpname, $this->tmpname);
        $this->assertNotEquals(0, $userid);

        $this->user->deleteUser($userid);

    }

}