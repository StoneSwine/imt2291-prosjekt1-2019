<?php

/*
 * Requirements:
 * NB: require once
 *
 * realpath( dirname( __FILE__ ) ).
 * This has to be there to get the right context for the path to the /src directory
 */
require_once realpath( dirname( __FILE__ ) ). "/../../../src/classes/DB.php";

/**
 * Class DBTest tests that the the database connection is set up properly
 */
class DBTest extends \Codeception\Test\Unit
{
    /**
     * @var \UnitTester
     */
    protected $tester;
    
    protected function _before(){
    }

    protected function _after(){
    }

    /**
     * Test for database connection to video database
     */
    public function testGetVideoDBConnection(){
        $this->assertInstanceOf(
            PDO::class, DB::getVideoDBConnection()
        );
    }

    /**
     * Test for database connection to Accounts database
     */
    public function testGetAccountsDBConnection(){
        $this->assertInstanceOf(
            PDO::class, DB::getAccountsDBConnection()
        );
    }

    // TODO: Teste skrive og lese fra database



    /**
     * Tests that the connection to the database is established correctly
     */
    /*public function testDBConnection(){
        $this->assertInstanceOf(
            PDO::class,
            DB::getDBConnection()
        );
    }*/
}