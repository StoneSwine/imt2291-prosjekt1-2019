<?php

use \Codeception\Util\Locator;
class PlaylistCest
{
    public function _before(AcceptanceTester $I)
    {
        if($I->loadSessionSnapshot('login')){
            return;
        }
        $I->wantToTest("No testing. just login and get authorized");
        $I->amOnPage('/login.php');
        $I->fillField("inputEmail", "kyrre@docker.com");
        $I->fillField("inputPassword", new \Codeception\Step\Argument\PasswordArgument('admin'));
        //$I->checkOption("input[type=checkbox]");
        $I->click('submit');
        $I->see("logout");
        $I->amOnPage("index.php");
        $I->saveSessionSnapshot('login');
    }


//    private function loginFirstPlease(AcceptanceTester $I){


  //  }

    // Tests
    /**
     * Create a playlist
     * @param AcceptanceTester $I
     */
    public function createPlaylist(AcceptanceTester $I){
        $I->wantToTest("Create playlist");
       // $I->loginFirstPlease();
        $I->amOnPage('playlist.php');

        $I->see("Lag ny spilleliste");

        //Fill form
        $I->fillField('playlisttitle', 'Best Of Jian Yang');
        $I->fillField('playlistdescription', 'The best clips from Jian Yang from Silicon Valley');
        //$I->fillField('course', '');
        $I->attachFile('thumbnail', 'php_meme_1.jpg');

        $I->click('#newplaylist');
    }

    /**
     * Add video to playlist
     * @depends createPlaylist
     * @param AcceptanceTester $I
     */
    public function addVideosToPlaylist(AcceptanceTester $I){
        $I->wantToTest("Upload video to playlist");
        //$I->loginFirstPlease();
        $eatFish = "JianYangEatsFish.mp4";
        $eatFish_thumbnail = "JianYangEatsFish_thumbnail.png";

        $businessTrip = "GuysGoOnABusinessTrip.mp4";
        $businessTrip_thumbnail = "GuysGoOnABusinessTrip_thumbnail.png";

        $trash = "trash.mp4";
        $trash_thumbnail = "trash_thumbnail.png";


        // Add first video
        $I->amOnPage("upload.php");
        $I->see("Upload video");

        $I->fillField("title","Jian Yang Eats Fish");
        $I->fillField("description","Eric Bachman is mad at Jian Yang for not cleaning up the kitchen.");
        //$I->selectOption("form select[name=playlist]","Best Of Jian Yang");
        $I->selectOption("//form/div/div/select[@name='playlist']","Best Of Jian Yang");
        $I->attachFile('//form[@id="uploadVideo"]/div/div/input[@name="video"]', $eatFish);
        $I->attachFile('//form[@id="uploadVideo"]/div/div/input[@name="thumbnail"]', $eatFish_thumbnail);
        $I->makeScreenshot("upload_1");
        $I->click("All done! Upload video");

        // Add second video
        $I->amOnPage("upload.php");
        $I->see("Upload video");

        $I->fillField("title","Guys Go On A Business Trip");
        $I->fillField("description","Eric Bachman prepares Jian Yang before going o a business trip");
        //$I->selectOption("form select[name=playlist]","Best Of Jian Yang");
        $I->selectOption("//form/div/div/select[@name='playlist']","Best Of Jian Yang");
        $I->attachFile('//form[@id="uploadVideo"]/div/div/input[@name="video"]', $businessTrip);
        $I->attachFile('//form[@id="uploadVideo"]/div/div/input[@name="thumbnail"]', $businessTrip_thumbnail);
        $I->makeScreenshot("upload_2");
        $I->click("All done! Upload video");

        // Add third video
        $I->amOnPage("upload.php");
        $I->see("Upload video");

        $I->fillField("title","Eric Bachman teaches Jian Yang to empty trash");
        $I->fillField("description","Jian Yang doesn't care about trash cans.");
        //$I->selectOption("form select[name=playlist]","Best Of Jian Yang");
        $I->selectOption("//form/div/div/select[@name='playlist']","Best Of Jian Yang");
        $I->attachFile('//form[@id="uploadVideo"]/div/div/input[@name="video"]', $trash);
        $I->attachFile('//form[@id="uploadVideo"]/div/div/input[@name="thumbnail"]', $trash_thumbnail);
        $I->makeScreenshot("upload_3");
        $I->click("All done! Upload video");

        // Check if they were added

        $I->amOnPage("playlist.php?user=2");

        //$I->see("","Best Of Jian Yang");
        //$x("//h5[@class='card-title' and text() = 'Best Of Jian Yang']/../a/@href");
        $I->click("//h5[@class='card-title' and text() = 'Best Of Jian Yang']/../a");
        // We should be on the playlist page now.
        // We should see our videos
        $I->see("Jian Yang Eats Fish");
        $I->see("Guys Go On A Business Trip");
        $I->see("Eric Bachman teaches Jian Yang to empty trash");
    }

    /**
     * Rotere på de tre videoene som ble opprettet i spillelista
     * @depends createPlaylist
     * @depends addVideosToPlaylist
     * @param AcceptanceTester $I
     */
    public function rotateVideos(AcceptanceTester $I){
        $I->wantToTest("Rotate videos in playlist");
        //$I->loginFirstPlease();
        // First navigate to the correct page
        $I->amOnPage("/playlist.php?user=2");
        // Go to the playlist page
        $I->click("//h5[@class='card-title' and text() = 'Best Of Jian Yang']/../a");
        // Now we should see three videos in the playlist "Best Of Jian Yang"

        //$x("//h5[text() = 'Erlich Bachman this is your mom']/../form/input[@name = 'changePosition']");
        // Videos are in position 1 2 3 from when we added them

        //                                      Change Jian Yang Eats Fish to position 3 (from 1)
        //$I->makeScreenshot("_before 1-3");
        $I->seeOptionIsSelected("//h5[text() = 'Jian Yang Eats Fish']/../div/form/input[@name = 'changePosition']/../select", "1");

        $I->selectOption("//h5[text() = 'Jian Yang Eats Fish']/../div/form/input[@name = 'changePosition']/../select", '3');
        // Page should be updated
        $I->seeOptionIsSelected("//h5[text() = 'Jian Yang Eats Fish']/../div/form/input[@name = 'changePosition']/../select", "3");
        //$I->makeScreenshot("_after 1-3");

        //                                      Change Guys Go On A Business Trip to position 2 (from 1 got position 1 after first move)
        $I->seeOptionIsSelected("//h5[text() = 'Guys Go On A Business Trip']/../div/form/input[@name = 'changePosition']/../select", "1");

        $I->selectOption("//h5[text() = 'Guys Go On A Business Trip']/../div/form/input[@name = 'changePosition']/../select", '2');
        // Page should be updated
        $I->seeOptionIsSelected("//h5[text() = 'Guys Go On A Business Trip']/../div/form/input[@name = 'changePosition']/../select", "2");

        //                                      Change Eric Bachman teaches Jian Yang to empty trash to position 2 (from 1 - moved after second move)
        $I->seeOptionIsSelected("//h5[text() = 'Eric Bachman teaches Jian Yang to empty trash']/../div/form/input[@name = 'changePosition']/../select", "1");
        $I->selectOption("//h5[text() = 'Eric Bachman teaches Jian Yang to empty trash']/../div/form/input[@name = 'changePosition']/../select", '2');
        // Page should be updated
        $I->seeOptionIsSelected("//h5[text() = 'Eric Bachman teaches Jian Yang to empty trash']/../div/form/input[@name = 'changePosition']/../select", "2");
    }

}
